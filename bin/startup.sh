#!/bin/sh
if [ $# -eq 1 ]; then
  # 项目名称
  project_name=$1
  project_name=${project_name%.*}
  #环境变量
  jvm_opts="-Xms256m -Xmx256m -XX:PermSize=32m -XX:MaxPermSize=128m"
  #部署目录
  deploy_dir=/home/deploy/services
  #日志目录
  logs_dir=$deploy_dir/logs/${project_name}
  # 应用路径
  project_path=$deploy_dir/${project_name}.jar

  #检测目录
  if [[ ! -d "$deploy_dir" ]]; then
          mkdir -p ${deploy_dir}
  fi
  #检测目录
  if [[ ! -d "$logs_dir" ]]; then
          mkdir -p ${logs_dir}
  fi

  #日志路径
  logs_path=$logs_dir/stdout.out

  # 应用的进程ID
  pid=`ps -ef | grep java | grep ${project_name}.jar | grep -v grep | awk '{print $2}'`

  # 如果应用的进程存在, 杀死该进程
  if [[ -n "$pid" ]]; then
      echo -e "[WARN] 服务 ${project_name} 正在结束进程..."
      kill -9 ${pid}
      echo -e "[WARN] 服务 ${project_name} 已停止"
  fi
  #编译环境 防止java环境不生效
  source /etc/profile
  # 启动应用服务
  BUILD_ID=dontKillMe  nohup java ${jvm_opts} -jar ${project_path}  > ${logs_path} 2>&1 &

  echo -e "[INFO] 服务 ${project_name} 启动完成！ 请查看运行日志：tail -f ${logs_path}"

else
  echo -e '[ERROR] ------------------------------------------------------------------------'
  echo -e '[ERROR] 参数不正确！语法：startup.sh arg1(注：项目名称)'
  echo -e '[ERROR] ------------------------------------------------------------------------'
fi
