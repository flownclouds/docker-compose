#!/bin/bash
#每两小时清除一次缓存
echo "开始清除缓存..."
#写入硬盘，防止数据丢失
sync;sync;sync
#延迟10秒
sleep 10
echo 3 > /proc/sys/vm/drop_caches
echo "清理结束"
