#!/bin/sh
if [ $# -eq 1 ]; then

  # 项目名称
  project_name=$1
  project_name=${project_name%.*}
  # 应用的进程ID
  pid=`ps -ef | grep java | grep ${project_name}.jar | grep -v grep | awk '{print $2}'`

  # 如果应用的进程存在, 杀死该进程
  if [[ ! -n "$pid" ]]; then
     echo -e "[WARN] 服务 $project_name 已停止"
    else
     echo -e "[INFO] 服务 $project_name 正在运行...[${pid}]"
  fi
else
  echo -e '[ERROR] ------------------------------------------------------------------------\033[0m'
  echo -e '[ERROR] 参数不正确！语法：status.sh arg1(注：项目名称)'
  echo -e '[ERROR] ------------------------------------------------------------------------\033[0m'
fi

