#!/bin/bash
# 删除名称或标签为none的镜像
docker rmi -f $(docker images -a | grep '<none>' | awk '{print $3}')

# 删除所有未被任何容器关联引用的卷
docker volume rm $(docker volume ls -qf dangling=true)