#!/bin/sh
if [ $# -eq 1 ]; then

  # 项目名称
  project_name=$1
  project_name=${project_name%.*}
  #服务包目录
  package_dir=/home/package
   #部署目录
  deploy_dir=/home/services
  # 应用路径
  project_path=$deploy_dir/${project_name}.jar

  echo -e '[INFO] ------------------------------------------------------------------------'
  echo -e '[INFO] 正在部署应用，请稍后 . . .'
  echo -e '[INFO] ------------------------------------------------------------------------'

  sh /home/deploy/bin/stop.sh ${project_name}

  # 替换为最新的应用包
  cp -f ${package_dir}/${project_name}.jar ${project_path}

  # 执行启动脚本
  sh /home/deploy/bin/startup.sh ${project_name}

else
  echo -e '[ERROR] ------------------------------------------------------------------------'
fi
