#!/bin/bash
eg="sh docker_run.sh hub.docker.com username password redis:latest redis 6379:6379 prod nacos1:8848,nacos2:8848,nacos3:8848"
if [ ! -n "$1" ]; then
  echo "docker仓库地址不能为空!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$2" ]; then
  echo "docker仓库登录名!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$3" ]; then
  echo "docker仓库密码!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$4" ]; then
  echo "镜像名称不能为空!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$5" ]; then
  echo "容器名不能为空!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$6" ]; then
  echo "容器端口映射不能为空!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$7" ]; then
  echo "当前运行环境不能为空!"
  echo "示例:${eg}"
  exit 1
fi
if [ ! -n "$8" ]; then
  echo "NACOS地址不能为空!"
  echo "示例:${eg}"
  exit 1
fi
# docker 仓库地址
REGISTRY_URL=$1
# docker 仓库登录名
USERNAME=$2
# docker 仓库密码
PASSWORD=$3
# 镜像名称
IMAGE=$4
# 容器名
CONTAINER=$5
# 容器端口映射
CONTAINER_PORT=$6
# 当前运行环境
PROFILES=$7
# nacos服务地址
NACOS_SERVERS=$8
# JVM参数
JAVA_OPTS="-server -Xms128m -Xmx256m"

echo "登陆docker仓库"
docker login ${REGISTRY_URL} -u ${USERNAME} -p ${PASSWORD}

if [ $? -eq 0 ]; then
  echo "登陆成功"
else
  echo "登陆失败"
  exit 1
fi

echo "拉取指定镜像: ${IMAGE}"
docker pull ${IMAGE}

if [ $? -eq 0 ]; then
  echo "拉取镜像成功"
else
  echo "拉取镜像失败"
  exit 1
fi

# 更新容器操作

echo "先停止现有的容器：${CONTAINER}"
docker stop $(docker ps -a | grep ${CONTAINER} | awk '{print $1}')

if [ $? -eq 0 ]; then

  echo "停止容器成功"

  echo "正在删除旧的容器"
  docker rm -f $(docker ps -a | grep ${CONTAINER} | awk '{print $1}')
  echo "删除容器完成: ${backup}"
else
  echo "停止容器失败"
fi

echo "等待5秒..."
sleep 5

echo "启动容器: ${CONTAINER} "

# 启动命令
# docker run --env PROFILES="prod" --env JAVA_OPTS="-server -Xms128m -Xmx256m" --env NACOS_SERVERS="nacos1:8848,nacos2:8848,nacos3:8848" -d --restart=on-failure:3 --name=spring-cloud-eureka-server-demo -p 9011:9011 192.168.75.12/springcloud/spring-cloud-eureka-server-demo:0.1

docker run --env PROFILES="${PROFILES}" --env JAVA_OPTS="${JAVA_OPTS}" --env NACOS_SERVERS="${NACOS_SERVERS}" -d --restart=always --name=${CONTAINER} --net=host -p ${CONTAINER_PORT} ${IMAGE}

echo "等待启动完毕..."
sleep 60

if [ $? -eq 0 ]; then
  echo "更新容器成功"
else
  echo "更新容器失败"
  exit 1
fi
