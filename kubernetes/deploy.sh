#!/bin/bash
stty erase ^h

#判断docker是否已经被安装
if ! [ -x "$(command -v docker)" ]; then
 #安装依赖包
yum install -y yum-utils device-mapper-persistent-data lvm2
#设置docker源
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
#安装docker
yum install docker-ce containerd.io
#命令补全
yum -y install bash-completion
source /etc/profile.d/bash_completion.sh
#配置加速器
mkdir -p /etc/docker
tee /etc/docker/daemon.json <<-'EOF'
{
"registry-mirrors": [
        "https://mirror.ccs.tencentyun.com",
        "https://registry.docker-cn.com",
        "http://hub-mirror.c.163.com",
        "https://docker.mirrors.ustc.edu.cn"
        ],
"exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
#设置docker开机自启
systemctl enable docker
systemctl daemon-reload
#重新启动docker
systemctl restart docker
#查看docker已经安装成功
docker -v
echo "Successfully installed docker-ce"
else
  echo "docker has been installed"
fi

yum install openssl

# 关闭防火墙

systemctl disable firewalld

systemctl stop firewalld

# 关闭selinux

# 临时禁用selinux

setenforce 0

# 永久关闭 修改/etc/sysconfig/selinux文件设置

sed -i 's/SELINUX=permissive/SELINUX=disabled/' /etc/sysconfig/selinux

sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

# 禁用交换分区

swapoff -a

# 永久禁用，打开/etc/fstab注释掉swap那一行。

sed -i 's/.*swap.*/#&/' /etc/fstab

# 修改内核参数

cat <<EOF >  /etc/sysctl.d/k8s.conf

net.bridge.bridge-nf-call-ip6tables = 1

net.bridge.bridge-nf-call-iptables = 1

EOF

sysctl --system

#安装k8s
if ! [ -x "$(command -v kubectl)" ]; then
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

# 安装kubeadm、kubectl、kubelet

yum install -y kubectl-1.23.5-0 kubeadm-1.23.5-0 kubelet-1.23.5-0

# 启动kubelet服务

systemctl enable kubelet && systemctl start kubelet

echo "source <(kubectl completion bash)" >> ~/.bash_profile
source ~/.bash_profile
url=registry.cn-hangzhou.aliyuncs.com/google_containers
version=v1.23.5
images=(`kubeadm config images list --kubernetes-version=$version|awk -F '/' '{print $2}'`)
for imagename in ${images[@]} ; do
docker pull $url/$imagename
docker tag $url/$imagename k8s.gcr.io/$imagename
docker rmi -f $url/$imagename
done
echo "Successfully installed k8s "
else
	echo "kubectl has been installed"
fi

systemctl enable kubelet
systemctl enable kubeadm
systemctl enable kubectl

kubelet --version
kubeadm version

#安装git
if ! [ -x "$(command -v git)" ]; then
	yum install git
else
	echo "git has been installed"
fi


#判断是master还是node
echo "Is the server master or node :"
read answer



#部署Kubernetes Master
if [[ "$answer" = "master" ]]; then
    echo "your ip:"
    read serverip

    #查询镜像列表
    kubeadm config images list

    #下载镜像
    images=(
      kube-apiserver:v1.23.5
      kube-controller-manager:v1.23.5
      kube-scheduler:v1.23.5
      kube-proxy:v1.23.5
      pause:3.6
      etcd:3.5.1-0
      coredns:v1.8.6
    )

    for imageName in ${images[@]} ; do
        docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
        if [[ $imageName =~ "coredns" ]];then
            docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName k8s.gcr.io/coredns/$imageName
        else
           docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName k8s.gcr.io/$imageName
        fi
        docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
    done

    sudo kubeadm init --kubernetes-version=v1.23.5 --apiserver-advertise-address="$serverip" --pod-network-cidr=10.244.0.0/16

    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    export KUBECONFIG=$HOME/.kube/config

    # 部署网络插件fannel
    kubectl apply -f kube-flannel.yml

    # 检查状态
    kubectl get pods -n kube-system -l app=flannel

    # 安装kuboard 管理面板
    kubectl create -f kuboard-v3.yaml

    source ~/.bash_profile
    echo "Master Install Success!"
fi

#部署Kubernetes Node
if [[ "$answer" = "node" ]]; then
	echo "Master IP:"
    read masterip
    echo "Master token:"
    read token
    echo "ssh:"
    read ssh
    kubeadm join "$masterip":6443 --token="$token" --discovery-token-ca-cert-hash "$ssh"
    echo "Node join Success!"
fi



